# COVID_19_SRD

A set of files scraped from [Unidos' blog](url), cleaned and arranged specifically for free reuse.

## Repo structure
```
COVID_19_SRD/
│
├── data/
│   ├── covid19_srd.csv
│   ├── covid19_srd.geojson
├── out_img/
│   ├── covid19_srd.png
└── readme.md

```

## Preview

An [interactive map](https://rpubs.com/andriatz/covid19_srd) is available.

![A choropleth map based on this dataset](https://gitlab.com/andriatz/covid_19_srd/-/raw/master/out_img/covid19_srd.png)